<?php

namespace Freshinteractive\IterableSdk\Traits;

use Freshinteractive\IterableSdk\IterableSdk;
use Freshinteractive\IterableSdk\Utils\Config;

trait InteractsWithIterable
{
    /**
     * Fields to include in iterable.
     *
     * @return array
     */
    abstract public function iterableFields(): array;

    /**
     * @return string
     */
    abstract public function getIterableUserId(): string;

    /**
     * @return IterableSdk
     */
    public function iterable(): IterableSdk
    {
        return new IterableSdk();
    }

    /**
     * Maps Model's fields to iterable fields.
     *
     * @return array
     */
    public function getIterableUser(): array
    {
        $mappedFields = [];

        foreach ($this->iterableFields() as $key => $value) {
            $iterableKey = (is_string($key))
                ? $key
                : $value;

            $iterableValue = ($value instanceof \Closure)
                ? $value()
                : $this->{$value};

            if (isset($iterableValue)) {
                $mappedFields[$iterableKey] = $iterableValue;
            }
        }

        return $mappedFields;
    }

    /**
     * The iterableUser Attribute (for use with eloquent models).
     *
     * @return array
     */
    public function getIterableUserAttribute(): array
    {
        return $this->getIterableUser();
    }

    /**
     * @param $listId
     * @return mixed
     */
    public function toIterable($listId = null)
    {
        $listId = $listId ?? Config::config('iterable-sdk', 'listId');

        try {
            return $this->iterable()->addOrUpdateSubscriber($listId, $this);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function fromIterable()
    {
        try {
            return $this->iterable()->getUserByEmail($this->email);
        } catch (\Exception $e) {
            return false;
        }
    }
}
