<?php

namespace Freshinteractive\IterableSdk;

use Carbon\Carbon;
use Freshinteractive\IterableSdk\Contracts\HasIterable;
use Freshinteractive\IterableSdk\Http\Clients\IterableClient;
use Freshinteractive\IterableSdk\Utils\Config;
use Freshinteractive\IterableSdk\Utils\Regex;

class IterableSdk
{
    private IterableClient $client;
    private array $config;
    private bool $inProduction;
    private array $whitelistedEmails;

    /**
     * @throws \RuntimeException
     */
    public function __construct()
    {
        $this->config = Config::config('iterable-sdk');

        if (!isset($this->config['apiKey'])) {
            throw new \RuntimeException('Iterable Api Key must be set');
        }

        $this->client = new IterableClient($this->config['apiKey'], $this->config['debug'] ?? false);
        $this->inProduction = $this->config['inProduction'] ?? true;
        $this->whitelistedEmails = $this->config['whitelistedEmails'] ?? [];
    }

    /**
     * @param string $email
     * @throws \RuntimeException
     * @return void
     */
    private function isWhitelisted(string $email): void
    {
        if (!$this->inProduction && !Regex::pregInArray($email, $this->whitelistedEmails)) {
            throw new \RuntimeException('Iterable interaction is not approved for email "' . $email . '". App is not in production, and email is not whitelisted.');
        }
    }

    /**
     * @param $listId
     * @param HasIterable $subscriber
     * @param array|null $dataFields
     * @return false|mixed
     */
    public function addOrUpdateSubscriber($listId, HasIterable $subscriber, ?array $dataFields = null)
    {
        try {
            $this->isWhitelisted($subscriber->email);

            return $this->client->post('/api/lists/subscribe', [
                'listId' => (int) ($listId ?? $this->config['listId']),
                'subscribers' => [
                    [
                        'email' => $subscriber->email,
                        'dataFields' => $dataFields ?? $subscriber->getIterableUser() ?? [],
                        'userId' => $subscriber->getIterableUserId(),
                        'preferUserId' => $this->config['preferUserId'] ?? true,
                        'mergeNestedObjects' => $this->config['mergeNestedObjects'] ?? true
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $email
     * @param $listToRemove
     * @return false|mixed
     */
    public function removeUserFromList(string $email, $listToRemove)
    {
        try {
            $this->isWhitelisted($email);

            $user = $this->getUserByEmail($email);

            $emailListIds = array_values(
                array_filter($user['emailListIds'] ?? [], function ($listId) use ($listToRemove) {
                    return $listId !== $listToRemove;
                })
            );

            return $this->client->post('/api/users/updateSubscriptions', [
                'email' => $email,
                'emailListIds' => $emailListIds
            ]);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string $email
     * @return array|mixed
     */
    public function getUserByEmail(string $email)
    {
        $response = $this->client->get('/api/users/' . $email);

        return $response['user']['dataFields'] ?? [];
    }

    /**
     * @param string $id
     * @return array|mixed
     */
    public function getUserById(string $id)
    {
        $response = $this->client->get('/api/users/byUserId/' . $id);

        return $response['user']['dataFields'] ?? [];
    }

    /**
     * @param string $event
     * @param array $data
     * @return false|mixed
     */
    public function triggerEvent(string $event, array $data = [])
    {
        try {
            if (isset($data['email'])) {
                $this->isWhitelisted($data['email']);
            }

            return $this->client->post('/api/events/track', array_merge(
                [ 'eventName' => $event ],
                $data
            ));
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param int $campaignId
     * @param string $email
     * @param array $dataFields
     * @param bool $allowRepeatMarketingSends
     * @param Carbon|null $sendAt
     * @param array|null $metaData
     * @return false|mixed
     */
    public function triggerEmail(
        int $campaignId,
        string $email,
        ?array $dataFields = null,
        bool $allowRepeatMarketingSends = true,
        Carbon $sendAt = null,
        ?array $metaData = null
    )
    {
        $body = [
            'campaignId' => $campaignId,
            'recipientEmail' => $email,
            'dataFields' => $dataFields,
            'allowRepeatMarketingSends' => $allowRepeatMarketingSends,
            'metadata' => $metaData
        ];

        if ($sendAt) {
            $sendAt->utc();

            $body['sendAt'] = $sendAt->toDateTimeString();
        }

        try {
            $this->isWhitelisted($email);

            return $this->client->post('/api/email/target', $body);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param int $workflowId
     * @param string $email
     * @param array|null $dataFields
     * @return false|mixed
     */
    public function triggerWorkflowByEmail(
        int $workflowId,
        string $email,
        ?array $dataFields = null
    )
    {
        try {
            $this->isWhitelisted($email);

            return $this->client->post('/api/workflows/triggerWorkflow', [
                'email' => $email,
                'workflowId' => $workflowId,
                'dataFields' => $dataFields
            ]);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getMessageTypes()
    {
        return $this->client->get('/api/messageTypes');
    }

    /**
     * @param string $email
     * @param array $body
     * @return false|mixed
     */
    public function updateUserSubscriptions(string $email, array $body)
    {
        try {
            $this->isWhitelisted($email);

            $body['email'] = $email;

            return $this->client->post('/api/users/updateSubscriptions', $body);
        } catch (\Exception $e) {
            return false;
        }
    }
}
