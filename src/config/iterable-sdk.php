<?php

return [
    'apiKey' => null,
    'listId' => '',
    'debug' => false,
    'inProduction' => true,
    'whitelistedEmails' => [],
    'preferUserId' => true,
    'mergeNestedObjects' => true
];
