<?php

namespace Freshinteractive\IterableSdk\Utils;

use Composer\Autoload\ClassLoader;

class Config
{
    /**
     * @param string $fileName
     * @return string
     */
    public static function getConfigFile(string $fileName): string
    {
        $reflection = new \ReflectionClass(ClassLoader::class);
        $rootDir = dirname($reflection->getFileName(), 2);

        $rootConfig = $rootDir . '/../config/' . $fileName . '.php';
        $defaultConfig = __DIR__ . '/../config/' . $fileName . '.php';

        return (file_exists($rootConfig))
            ? $rootConfig
            : $defaultConfig;
    }

    /**
     * @param string $fileName
     * @param string|null $key
     * @return mixed|null
     */
    public static function config(string $fileName, ?string $key = null)
    {
        $configs = include(self::getConfigFile($fileName));

        return $key
            ? $configs[$key] ?? null
            : $configs;
    }
}
