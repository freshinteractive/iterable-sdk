<?php

namespace Freshinteractive\IterableSdk\Utils;

class Regex
{
    /**
     * @param string $string
     * @return bool
     */
    public static function isRegex(string $string): bool
    {
        return preg_match('/^\/.+\/[a-z]*$/i', $string);
    }

    /**
     * @param string $string
     * @param array $patterns
     * @return bool
     */
    public static function pregInArray(string $string, array $patterns): bool
    {
        foreach($patterns as $pattern) {
            if ($pattern === $string ||
                (self::isRegex($pattern) && preg_match($pattern, $string))
            ) {
                return true;
            }
        }

        return false;
    }
}
