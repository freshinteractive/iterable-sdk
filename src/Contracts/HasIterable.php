<?php

namespace Freshinteractive\IterableSdk\Contracts;

use Freshinteractive\IterableSdk\IterableSdk;

/**
 * @property string $email
 */
interface HasIterable
{
    public function iterableFields(): array;

    public function iterable(): IterableSdk;

    public function getIterableUserId(): string;

    public function getIterableUser(): array;

    public function getIterableUserAttribute(): array;

    public function toIterable();

    public function fromIterable();
}
