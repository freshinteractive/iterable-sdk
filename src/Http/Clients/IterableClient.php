<?php

namespace Freshinteractive\IterableSdk\Http\Clients;

use GuzzleHttp\Client;

class IterableClient
{
    private Client $client;
    private string $apiKey;
    private bool $debug;

    /**
     * @param string $apiKey
     * @param bool $debug
     */
    public function __construct(string $apiKey, bool $debug = false)
    {
        $this->apiKey = $apiKey;
        $this->debug = $debug;
        $this->client = new Client([
            'headers' => [
                'Content-Type' => 'application/json',
                'Api_Key' => $this->apiKey,
                'debug' => $this->debug
            ],
            'base_uri' => 'https://api.iterable.com'
        ]);
    }

    /**
     * @param string $uri
     * @param array $query
     * @return mixed
     */
    public function get(string $uri, array $query = [])
    {
        $response = $this->client->get($uri, [ 'query' => $query ]);

        return json_decode($response->getBody(), true);
    }

    /**
     * @param string $uri
     * @param array $body
     * @return mixed
     */
    public function post(string $uri, array $body = [])
    {
        $response = $this->client->post($uri, [ 'json' => $body ]);

        return json_decode($response->getBody(), true);
    }
}
